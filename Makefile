


SRC := tests
BIN := bin
DOC := docs

CXXFLAGS := -std=c++14 -pthread -O0 -g3 -D RANDOMLY_USEFUL_ASSERT_THROWS
CPPFLAGS := -Iinclude

all: $(BIN) $(BIN)/test docs

$(BIN):
	@mkdir -p $(BIN)

docs: $(DOC)/html/index.html Doxyfile
	$(RM) -r $(DOC)/*
	doxygen

$(DOC)/html/index.html: include/randomly_useful.hpp Doxyfile

$(BIN)/test: $(SRC)/test.cpp
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) -o $@ $< $(LDFLAGS) $(LDLIBS)

run-tests: $(BIN)/test
	@./$(BIN)/test

clean:
	$(RM) $(BIN)/test

.PHONY: docs
#ifndef RANDOMLY_USEFUL_GLOBAL_MERSENNE_TWISTER_64_HPP
#define RANDOMLY_USEFUL_GLOBAL_MERSENNE_TWISTER_64_HPP
/**

MIT License

Copyright (c) 2021 Galik <galik.bool@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

**/

/*
 * mersenne_twister.hpp
 *
 *  Created on: 18 Apr 2021
 *      Author: galik
 */

#include "PRNG.hpp"

namespace randomly_useful {
namespace mersenne_twister_64 {

inline static_mt19937_64_prng prng;

#include "global_template.tmp"

} // namespace mersenne_twister_6
} // namespace randomly_useful

#endif // RANDOMLY_USEFUL_GLOBAL_MERSENNE_TWISTER_64_HPP

#ifndef RANDOMLY_USEFUL_RANDOMLY_USEFUL_HPP
#define RANDOMLY_USEFUL_RANDOMLY_USEFUL_HPP
/**

MIT License

Copyright (c) 2018 Galik <galik.bool@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

**/

/*
 * randomly_useful.hpp
 *
 *  Created on: 1 Jul 2018
 *      Author: galik
 */

// TODO: add static_asserts for random_number() et al.

#include <algorithm>
#include <array>
#include <cassert>
#include <random>
#include <thread>
#include <type_traits>

#if __cplusplus >= 201703L
#  define RANDOMLY_USEFUL_AT_LEAST_CPP17
#endif

#ifdef RANDOMLY_USEFUL_AT_LEAST_CPP17
//#include "uniform_int_distribution_fast.hpp" // repeatability
#endif

#ifndef NDEBUG
#ifndef RANDOMLY_USEFUL_ASSERT_THROWS
#include <iostream>
#  define RANDOMLY_USEFUL_ASSERT_ACTION(msg) do { \
	std::cerr << msg << '\n'; \
	std::terminate(); \
	} while(0)
#else
#include <sstream>
#include <stdexcept>
#  define RANDOMLY_USEFUL_ASSERT_ACTION(msg) \
	do { \
	std::ostringstream o; \
	o << msg; \
	throw std::runtime_error(o.str()); \
	}while(0)
#endif
#endif // NDEBUG

#ifdef NDEBUG
#define RANDOMLY_USEFUL_ASSERT(expr) do{}while(0)
#else
#define RANDOMLY_USEFUL_ASSERT(expr) \
do { \
	if(!(expr)) \
	{ \
		RANDOMLY_USEFUL_ASSERT_ACTION("assertion '" << #expr << "' failed." << "\nfile: " << __FILE__ << "\nline: " << __LINE__ << '\n'); \
	} \
}while(0)
#endif // NDEBUG

namespace randomly_useful {

/*! \struct std_int_type_test
	\brief SFNAE test for integer types
 */
template<typename Integer>
struct std_int_type_test
{
	constexpr static bool value =
	std::is_same<Integer, short>::value
	|| std::is_same<Integer, int>::value
	|| std::is_same<Integer, long>::value
	|| std::is_same<Integer, long long>::value
	|| std::is_same<Integer, unsigned short>::value
	|| std::is_same<Integer, unsigned int>::value
	|| std::is_same<Integer, unsigned long>::value
	|| std::is_same<Integer, unsigned long long>::value;
};

/*! \struct std_real_type_test
	\brief SFNAE test for float types
 */
template<typename Real>
struct std_real_type_test
{
	constexpr static bool value =
	std::is_same<Real, float>::value
	|| std::is_same<Real, double>::value
	|| std::is_same<Real, long double>::value;
};

// uniform_int_distribution is consistent on different platforms
// from C++17 onwards
#ifdef RANDOMLY_USEFUL_AT_LEAST_CPP17
template<typename Integer = int>
//using uniform_int_distribution = ext::uniform_int_distribution_fast<Integer>;
using uniform_int_distribution = std::uniform_int_distribution<Integer>;
#else
template<typename Integer = int>
using uniform_int_distribution = std::uniform_int_distribution<Integer>;
#endif

template<typename Container>
using enable_random_access_container =
	std::enable_if<std::is_same<typename std::remove_reference<decltype(Container()[0])>::type,
		typename Container::value_type>::value>;

template<typename GeneratorSource>
class PRNG
{
public:
	PRNG()
	{
		using gen_type = typename GeneratorSource::generator_type;
		using dev_type = typename GeneratorSource::random_device_type;
		using res_type = typename GeneratorSource::random_device_type::result_type;
		auto constexpr gen_size = sizeof(gen_type)/sizeof(res_type);
		std::array<res_type, gen_size> data;
		dev_type rd{};
		std::generate(std::begin(data), std::end(data), std::ref(rd));
		std::seed_seq seeds(std::begin(data), std::end(data));
		generator().seed(seeds);
	}

	PRNG(std::seed_seq& seeds)
	{
		generator().seed(seeds);
	}

	template<typename Number, typename... Numbers>
	PRNG(Number number, Numbers... numbers)
	{
		std::seed_seq seeds({number, numbers...});
		generator().seed(seeds);
	}

	template<typename Number>
	PRNG(Number number)
	{
		generator().seed(typename GeneratorSource::generator_type::result_type(number));
	}

	/*! \fn random_generator
	    \brief Get the underlying pseudo random number generator.

	    \return The random number generator used by this library.
	*/
	auto& generator()
	{
		return detail.generator();
	}

	/**
	 * Properly re-seed the underlying pseudo random number
	 * generator from the std::random_device.
	 */
	void seed()
	{
		using gen_type = typename GeneratorSource::generator_type;
		using dev_type = typename GeneratorSource::random_device_type;
		using res_type = typename GeneratorSource::random_device_type::result_type;
		auto constexpr gen_size = sizeof(gen_type)/sizeof(res_type);
		std::array<res_type, gen_size> data;
		dev_type rd{};
		std::generate(std::begin(data), std::end(data), std::ref(rd));
		std::seed_seq seeds(std::begin(data), std::end(data));
		generator().seed(seeds);
	}

	/*!
	 * \brief Re-seed the underlying pseudo random number generator
	 * from a `std::seed_seq`.
	 * Every unique list of seeds value will cause the same stream
	 * of pseudo random numbers to be generated
	 *
	 * \param seeds The `std::seed_seq` to use.
	 */
	void seed(std::seed_seq& seeds)
	{
		generator().seed(seeds);
	}

	/**
	 * Seed the underlying pseudo random number generator.
	 * Every unique list of seeds value will cause the same stream
	 * of pseudo random numbers to be generated
	 *
	 * @param numbers The list of seed values to use.
	 */
	template<typename Number, typename... Numbers>
	void seed(Number number, Numbers... numbers)
	{
		std::seed_seq seeds({number, numbers...});
		generator().seed(seeds);
	}

	template<typename Number>
	void seed(Number number)
	{
		generator().seed(typename GeneratorSource::generator_type::result_type(number));
	}

	/**
	 * Generate a pseudo random number that is uniformly distributed
	 * between two values.
	 *
	 * @param from Lowest possible value.
	 * @param to Highest possible value.
	 *
	 * @return A random number between `from` and `to`
	 * (inclusive for integers, exclusive for floats)
	 */
	template<typename Number>
	Number random_number(Number from, Number to)
	{
		auto mm = std::minmax(from, to);
		return detail_random_number(mm.first, mm.second);
	}

	/**
	 * Generate a pseudo random number that is uniformly distributed
	 * between zero and the specified (positive or negative) value.
	 *
	 * @param to Highest possible value.
	 *
	 * @return A random number between `0` and `to` (inclusive)
	 */
	template<typename Number>
	Number random_number(Number to)
	{
		Number from = {};
		auto mm = std::minmax(from, to);
		return detail_random_number(mm.first, mm.second);
	}

	/**
	 * Generate a pseudo random number that is uniformly distributed
	 * between 0.0 and 1.0 (exclusive) for real values and 0 and the maximum
	 * possible value (inclusive) for integer numbers.
	 *
	 * @param Number the type of number to generate.
	 *
	 * @return A random number between 0.0 and 1.0
	 */
	template<typename Number>
	Number random_number()
	{
		return detail_random_number<Number>();
	}

	/**
	 * Randomly return true or false.
	 *
	 * @param p The probability that the function will return `true`.
	 *
	 * @return true or false
	 */
	template<typename Real = double>
	bool random_choice(Real p = 0.5)
	{
		RANDOMLY_USEFUL_ASSERT(p >= 0.0);
		RANDOMLY_USEFUL_ASSERT(p <= 1.0);
		return detail_randomly_distributed_number<std::bernoulli_distribution>(p);
	}

	/**
	 * Return an iterator to a randomly selected container element
	 * from the `begin` iterator up to, and including the `end` iterator.
	 *
	 * @param begin The first iterator in the range to be considered.
	 * @param end The last iterator in the range.
	 *
	 * @return An iterator to a pseudo randomly selected element
	 * from the supplied range (or the `end` iterator).
	 */
	template<typename Iter>
	Iter random_iterator(Iter begin, Iter end)
	{
		RANDOMLY_USEFUL_ASSERT(std::distance(begin, end) >= 0);
		return std::next(begin, random_number(std::distance(begin, end)));
	}

	/**
	 * Return a randomly selected iterator to container.
	 *
	 * @param c The container to select an iterator from.
	 *
	 * @return A randomly selected iterator, including std::end(c).
	 */
	template<typename Container>
	auto random_iterator(Container& c) -> decltype(std::begin(c))
	{
		RANDOMLY_USEFUL_ASSERT(!c.empty());
		return random_iterator(std::begin(c), std::end(c));
	}

	/**
	 * Return a randomly selected iterator to an array.
	 *
	 * @param array The array to select an element from.
	 *
	 * @return A randomly selected iterator, including std::end(array).
	 */
	template<typename T, std::size_t N>
	auto random_iterator(T(&array)[N]) -> decltype(std::begin(array))
	{
		RANDOMLY_USEFUL_ASSERT(N > 0);
		return std::next(std::begin(array), random_number(N));
	}

	/**
	 * Return a randomly selected element from a container. If the
	 * container is empty the behavior is undefined.
	 *
	 * @param c The container to select an element from.
	 *
	 * @return A reference to a pseudo randomly selected element from the supplied container.
	 */
	template<typename Container>
	auto random_element(Container&& c) -> decltype(*std::begin(c))
	{
		RANDOMLY_USEFUL_ASSERT(!c.empty());
		return *random_iterator(std::begin(std::forward<Container>(c)),
			std::prev(std::end(std::forward<Container>(c))));
	}

	/**
	 * Return a randomly selected element from a range of iterators. If the
	 * distance between the iterators is zero,  the behavior is undefined.
	 *
	 * @param begin The first iterator in the range to be considered.
	 * @param end The last iterator in the range which will not be included.
	 *
	 * @return A reference to a pseudo randomly selected element from the supplied container.
	 */
	template<typename Iter>
	auto random_element(Iter begin, Iter end) -> decltype(*begin)
	{
		RANDOMLY_USEFUL_ASSERT(std::distance(begin, end) > 0);
		return *random_iterator(begin, std::prev(end));
	}

	template<typename T, std::size_t N>
	auto random_element(T(&array)[N]) -> T&
	{
		RANDOMLY_USEFUL_ASSERT(N > 0);
		return array[random_number(N - 1)];
	}

	template<typename Iter>
	void random_shuffle(Iter begin, Iter end)
	{
		RANDOMLY_USEFUL_ASSERT(std::distance(begin, end) >= 0);
		std::shuffle(begin, end, generator());
	}

	template<typename Container, class = enable_random_access_container<Container>>
	void random_shuffle(Container& c)
	{
		static_assert(!std::is_const<Container>::value, "Parameter type must not be const.");
		std::shuffle(std::begin(c), std::end(c), generator());
	}

	template<typename T, std::size_t N>
	void random_shuffle(T(&array)[N])
	{
		RANDOMLY_USEFUL_ASSERT(N > 0);
		std::shuffle(std::begin(array), std::end(array), generator());
	}

	template<typename Duration1, typename Duration2>
	void random_sleep_between(Duration1 min, Duration2 max)
	{
		using common_duration_type = typename std::common_type<Duration1, Duration2>::type;
		auto duration = common_duration_type(random_number(min.count(), max.count()));
		std::this_thread::sleep_for(duration);
	}

	template<typename Duration>
	void random_sleep_upto(Duration duration)
		{ random_sleep_between(Duration(0), duration); }

	#ifdef RANDOMLY_USEFUL_AT_LEAST_CPP17

	template<typename PIter, typename SIter>
	auto random_sample(PIter begin, PIter end, SIter out, std::size_t n)
		{ std::sample(begin, end, out, n, generator()); }

	template<typename Container>
	auto random_sample(Container&& c, std::size_t n)
	{
		if(n >= c.size())
			return c;

		typename std::remove_reference<Container>::type s;
		s.reserve(n);
		random_sample(std::begin(std::forward<Container>(c)), std::end(std::forward<Container>(c)),
			std::inserter(s, std::end(s)), n);
		return s;
	}

	#endif // C++17

	// Distributions

	inline
	bool random_bernoulli(double p = 0.5)
	{
		return detail_randomly_distributed_number<std::bernoulli_distribution>(p);
	}

	template<typename Integer = int>
	Integer random_binomial(Integer t = 1, double p = 0.5)
	{
		static_assert(std_int_type_test<Integer>::value,
			"Template parameter must be short, int, long, long long, "
				"unsigned short, unsigned int, unsigned long, or unsigned long long");

		RANDOMLY_USEFUL_ASSERT(0 <= t);
		RANDOMLY_USEFUL_ASSERT(0.0 < p);
		RANDOMLY_USEFUL_ASSERT(p <= 1.0);
		// Requires that 0 ≤ p ≤ 1 and 0 ≤ t.
		return detail_randomly_distributed_number<std::binomial_distribution<Integer>>(t, p);
	}

	template<typename Integer = int>
	Integer random_negative_binomial(Integer k = 1, double p = 0.5)
	{
		static_assert(std_int_type_test<Integer>::value,
			"Template parameter must be short, int, long, long long, "
				"unsigned short, unsigned int, unsigned long, or unsigned long long");

		RANDOMLY_USEFUL_ASSERT(0 < k);
		RANDOMLY_USEFUL_ASSERT(0.0 < p);
		RANDOMLY_USEFUL_ASSERT(p <= 1.0);

		return detail_randomly_distributed_number<std::negative_binomial_distribution<Integer>>(k, p);
	}

	template<typename Integer = int>
	Integer random_geometric(double p = 0.5)
	{
		static_assert(std_int_type_test<Integer>::value,
			"Template parameter must be short, int, long, long long, "
				"unsigned short, unsigned int, unsigned long, or unsigned long long");

		RANDOMLY_USEFUL_ASSERT(p > 0.0);
		RANDOMLY_USEFUL_ASSERT(p < 1.0);

		return detail_randomly_distributed_number<std::geometric_distribution<Integer>>(p);
	}

	template<typename Integer = int>
	Integer random_poisson(double mean = 1.0)
	{
		static_assert(std_int_type_test<Integer>::value,
			"Template parameter must be short, int, long, long long, "
				"unsigned short, unsigned int, unsigned long, or unsigned long long");

		RANDOMLY_USEFUL_ASSERT(0.0 < mean);

		return detail_randomly_distributed_number<std::poisson_distribution<Integer>>(mean);
	}

	template<typename Real = double>
	Real random_exponential(Real lambda = 1.0)
	{
		static_assert(std_real_type_test<Real>::value,
			"Parameter must be float, double or long double");

		RANDOMLY_USEFUL_ASSERT(0.0 < lambda);

		return detail_randomly_distributed_number<std::exponential_distribution<Real>>(lambda);
	}

	template<typename Real = double>
	Real random_gamma(Real alpha = 1.0, Real beta = 1.0)
	{
		static_assert(std_real_type_test<Real>::value,
			"Parameters must be float, double or long double");

		return detail_randomly_distributed_number<std::gamma_distribution<Real>>(alpha, beta);
	}

	template<typename Real = double>
	Real random_weibull(Real a = 1.0, Real b = 1.0)
	{
		static_assert(std_real_type_test<Real>::value,
			"Parameters must be float, double or long double");

		return detail_randomly_distributed_number<std::weibull_distribution<Real>>(a, b);
	}

	template<typename Real = double>
	Real random_extreme_value(Real a = 0.0, Real b = 1.0)
	{
		static_assert(std_real_type_test<Real>::value,
			"Parameters must be float, double or long double");

		return detail_randomly_distributed_number<std::extreme_value_distribution<Real>>(a, b);
	}

	template<typename Real = double>
	Real random_normal(Real mean = 0.0, Real standard_deviation = 1.0)
	{
		static_assert(std_real_type_test<Real>::value,
			"Parameters must be float, double or long double");

		return detail_randomly_distributed_number<std::normal_distribution<Real>>(mean, standard_deviation);
	}

	template<typename Real = double>
	Real random_lognormal(Real m = 0.0, Real s = 1.0)
	{
		static_assert(std_real_type_test<Real>::value,
			"Parameters must be float, double or long double");

		return detail_randomly_distributed_number<std::lognormal_distribution<Real>>(m, s);
	}

	template<typename Real = double>
	Real random_chi_squared(Real n = 1.0)
	{
		static_assert(std_real_type_test<Real>::value,
			"Parameters must be float, double or long double");

		return detail_randomly_distributed_number<std::chi_squared_distribution<Real>>(n);
	}

	template<typename Real = double>
	Real random_cauchy(Real a = 0.0, Real b = 1.0)
	{
		static_assert(std_real_type_test<Real>::value,
			"Parameters must be float, double or long double");

		return detail_randomly_distributed_number<std::cauchy_distribution<Real>>(a, b);
	}

	template<typename Real = double>
	Real random_fisher_f(Real m = 1.0, Real n = 1.0)
	{
		static_assert(std_real_type_test<Real>::value,
			"Parameters must be float, double or long double");

		return detail_randomly_distributed_number<std::fisher_f_distribution<Real>>(m, n);
	}

	template<typename Real = double>
	Real random_student_t(Real n = 1.0)
	{
		static_assert(std_real_type_test<Real>::value,
			"Parameters must be float, double or long double");

		return detail_randomly_distributed_number<std::student_t_distribution<Real>>(n);
	}

	template<typename Integer = int>
	Integer random_discrete(std::initializer_list<double> doubles)
	{
		static_assert(std_int_type_test<Integer>::value,
			"Template parameter must be short, int, long, long long, "
				"unsigned short, unsigned int, unsigned long, or unsigned long long");

		return detail_randomly_distributed_number<std::discrete_distribution<Integer>>(doubles);
	}

	template<typename Integer = int, typename InputIt>
	Integer random_discrete(InputIt first, InputIt last)
	{
		static_assert(std_int_type_test<Integer>::value,
			"Template parameter must be short, int, long, long long, "
				"unsigned short, unsigned int, unsigned long, or unsigned long long");

		static_assert(std::is_convertible<
			typename std::iterator_traits<InputIt>::value_type, double>::value,
				"The iterator parameter's value types must be convertable to type double");

		return detail_randomly_distributed_number<std::discrete_distribution<Integer>>(first, last);
	}

	// http://en.cppreference.com/w/cpp/numeric/random/discrete_distribution
	//
	template<typename Integer = int, typename UnaryOperation>
	Integer random_discrete(std::size_t count, double xmin, double xmax, UnaryOperation unary_op)
	{
		static_assert(std_int_type_test<Integer>::value,
			"Template parameter must be short, int, long, long long, "
				"unsigned short, unsigned int, unsigned long, or unsigned long long");

		if(count == 0)
			return 0;

		double delta = (xmax - xmin) / double(count);

		RANDOMLY_USEFUL_ASSERT(delta > 0.0);

		return detail_randomly_distributed_number<std::discrete_distribution<Integer>>(
			count, xmin, xmax, unary_op);
	}

	// piecewise_constant_distribution
	// http://en.cppreference.com/w/cpp/numeric/random/piecewise_constant_distribution

	template<typename Real = double, typename InputIteratorB, typename InputIteratorW>
	Real random_piecewise_constant(InputIteratorB first_b, InputIteratorB last_b, InputIteratorW first_w)
	{
		static_assert(std_real_type_test<Real>::value,
			"Parameters must be float, double or long double");

		static_assert(
			std::is_convertible<typename std::iterator_traits<InputIteratorB>::value_type, double>::value
			||std::is_convertible<typename std::iterator_traits<InputIteratorW>::value_type, double>::value,
			"Iterator value_type must be convertible to type double");

		using dist_type = std::piecewise_constant_distribution<Real>;

		return detail_randomly_distributed_number<dist_type>(first_b, last_b, first_w);
	}

	template<typename Real = double, typename UnaryOperation>
	Real random_piecewise_constant(std::initializer_list<Real> bl, UnaryOperation fw)
	{
		static_assert(std_real_type_test<Real>::value,
			"Parameters must be float, double or long double");

		return detail_randomly_distributed_number<std::piecewise_constant_distribution<Real>>(bl, fw);
	}

	template<typename Real = double, typename UnaryOperation>
	Real random_piecewise_constant(std::size_t nw, Real xmin, Real xmax, UnaryOperation fw)
	{
		static_assert(std_real_type_test<Real>::value,
			"Parameters must be float, double or long double");

		static_assert(
			std::is_convertible<decltype(fw(0.0)), double>::value,
			"Functor return type must be convertible to type double");

		using dist_type = std::piecewise_constant_distribution<Real>;

		RANDOMLY_USEFUL_ASSERT(xmin < xmax);
		RANDOMLY_USEFUL_ASSERT(0.0 < (xmax - xmin) / Real(nw ? nw : 1));

		return detail_randomly_distributed_number<dist_type>(nw, xmin, xmax, fw);
	}

	// piecewise_linear_distribution
	// C++11 26.5.8.6.3
	//

	template<typename Real = double, typename InputIteratorB, typename InputIteratorW>
	Real random_piecewise_linear(InputIteratorB first_b, InputIteratorB last_b, InputIteratorW first_w)
	{
		static_assert(std_real_type_test<Real>::value,
			"Parameters must be float, double or long double");

		static_assert(
			std::is_convertible<typename std::iterator_traits<InputIteratorB>::value_type, double>::value
			||std::is_convertible<typename std::iterator_traits<InputIteratorW>::value_type, double>::value,
			"Iterator value_type must be convertible to type double");

		using dist_type = std::piecewise_linear_distribution<Real>;

		return detail_randomly_distributed_number<dist_type>(first_b, last_b, first_w);
	}

	template<typename Real = double, typename UnaryOperation>
	Real random_piecewise_linear(std::initializer_list<Real> bl, UnaryOperation fw)
	{
		static_assert(std_real_type_test<Real>::value,
			"Parameters must be float, double or long double");

		static_assert(
			std::is_convertible<decltype(fw(0.0)), double>::value,
			"Functor return type must be convertible to type double");

		using dist_type = std::piecewise_linear_distribution<Real>;

		return detail_randomly_distributed_number<dist_type>(bl, fw);
	}

	template<typename Real = double, typename UnaryOperation>
	Real random_piecewise_linear(std::size_t nw, Real xmin, Real xmax, UnaryOperation fw)
	{
		static_assert(std_real_type_test<Real>::value,
			"Parameters must be float, double or long double");

		static_assert(
			std::is_convertible<decltype(fw(0.0)), double>::value,
			"Functor return type must be convertible to type double");

		using dist_type = std::piecewise_linear_distribution<Real>;

		return detail_randomly_distributed_number<dist_type>(nw, xmin, xmax, fw);
	}

	template<typename Numeric>
	std::vector<Numeric> random_vector(std::size_t n, Numeric min, Numeric max)
	{
		std::vector<Numeric> c;
		c.reserve(n);
		std::generate_n(std::back_inserter(c), n, [&]{ return random_number(min, max); });
		return c;
	}


private:

	// TODO: Requires that from ≤ to and to - from ≤ std::numeric_limits<RealType>::max()
	template<typename Number>
	Number detail_random_number(Number from, Number to)
	{
		static_assert(std_int_type_test<Number>::value||std_real_type_test<Number>::value,
			"Parameters must be integer or floating point numbers");

		using Distribution = typename std::conditional
		<
			std::is_integral<Number>::value,
			uniform_int_distribution<Number>,
			std::uniform_real_distribution<Number>
		>::type;

		thread_local static Distribution dist;

		return dist(detail.generator(), typename Distribution::param_type{from, to});
	}

	template<typename Number>
	Number detail_random_number()
	{
		static_assert(std_int_type_test<Number>::value||std_real_type_test<Number>::value,
			"Parameters must be integer or floating point numbers");

		using Distribution = typename std::conditional
		<
			std::is_integral<Number>::value,
			uniform_int_distribution<Number>,
			std::uniform_real_distribution<Number>
		>::type;

		thread_local static Distribution dist;

		return dist(detail.generator());
	}

	template<typename Distribution, typename... Args>
	typename Distribution::result_type
	detail_randomly_distributed_number(Args&&... args)
	{
		using param_type = typename Distribution::param_type;

		thread_local static Distribution dist;
		return dist(detail.generator(), param_type(std::forward<Args>(args)...));
	}

	GeneratorSource detail;
};

template<typename Generator, typename RandomDevice = std::random_device>
class AutoGeneratorSource
{
public:
	using generator_type = Generator;
	using random_device_type = RandomDevice;

	Generator& generator() { return g; }

private:
	Generator g{RandomDevice{}()};
};

template<typename Generator, typename RandomDevice = std::random_device>
class ThreadLocalGeneratorSource
{
public:
	using generator_type = Generator;
	using random_device_type = RandomDevice;

	Generator& generator()
	{
		thread_local Generator g{RandomDevice{}()};
		return g;
	}
};

template<typename Generator, typename RandomDevice = std::random_device>
class StaticGeneratorSource
{
public:
	using generator_type = Generator;
	using random_device_type = RandomDevice;

	Generator& generator()
	{
		static Generator g{RandomDevice{}()};
		return g;
	}
};

template<typename Generator, typename RandomDevice = std::random_device>
using auto_prng = randomly_useful::PRNG<AutoGeneratorSource<Generator, RandomDevice>>;

template<typename Generator, typename RandomDevice = std::random_device>
using static_prng = randomly_useful::PRNG<StaticGeneratorSource<Generator, RandomDevice>>;

template<typename Generator, typename RandomDevice = std::random_device>
using thread_local_prng = randomly_useful::PRNG<ThreadLocalGeneratorSource<Generator, RandomDevice>>;

// Mersenne Twister
using auto_mt19937_prng = auto_prng<std::mt19937>;
using static_mt19937_prng = static_prng<std::mt19937>;
using thread_local_mt19937_prng = thread_local_prng<std::mt19937>;

using auto_mt19937_64_prng = auto_prng<std::mt19937_64>;
using static_mt19937_64_prng = static_prng<std::mt19937_64>;
using thread_local_mt19937_64_prng = thread_local_prng<std::mt19937_64>;

// LCG
using auto_minstd_prng = auto_prng<std::minstd_rand>;
using static_minstd_prng = static_prng<std::minstd_rand>;
using thread_local_minstd_prng = thread_local_prng<std::minstd_rand>;

// Subtract with carry
using auto_ranlux24_prng = auto_prng<std::ranlux24_base>;
using static_ranlux24_prng = static_prng<std::ranlux24_base>;
using thread_local_ranlux24_prng = thread_local_prng<std::ranlux24_base>;

using auto_ranlux48_prng = auto_prng<std::ranlux48_base>;
using static_ranlux48_prng = static_prng<std::ranlux48_base>;
using thread_local_ranlux48_prng = thread_local_prng<std::ranlux48_base>;

} // namespace randomly_useful

#endif // RANDOMLY_USEFUL_RANDOMLY_USEFUL_HPP

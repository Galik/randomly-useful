# Randomly Useful

## A useful (header only) library for random numbers.

This library seeks to make using random numbers for basic tasks easy(er) in `C++`. 
Since `C++11` and the new `<random>` capabilities, generating random numbers has been somewhat
involved.

```cpp
std::mt19937 mt{std::random_device{}()};
std::uniform_int_distribution<int> pick(0, 99);

auto n = pick(mt);
```

With this library a (pseudo) random number generator class is instantiated and 
then all the capabilities of the `<random>` library are easily on hand:

```cpp
#include <randomly_useful/PRNG.hpp>

namespace ru = randomly_useful;

// ...

ru::auto_mt19937_prng prng;

std::vector<int> v;

std::generate_n(std::back_inserter(v), 10, [&]{ return prng.random_number(0, 99); });

std::cout << "all: ";
for(auto i: v)
	std::cout << i << ' ';
std::cout << '\n';

std::cout << "random item: " << prng.random_element(v) << '\n';

auto r = std::minmax(prng.random_iterator(v), prng.random_iterator(v));

std::cout << "random range: ";
for(auto iter = r.first; iter != r.second; ++iter)
	std::cout << *iter << ' ';
std::cout << '\n';

std::cout << "random items: ";
for(auto i: v)
	if(prng.random_choice(0.5))
		std::cout << i << ' ';
std::cout << '\n';

```
### For even more convenience there are global generator headers

```cpp
#include <randomly_useful/global_mersenne_twister_64.hpp>

namespace ru { using namespace randomly_useful::mersenne_twister_64; }

// ...

std::cout << ru::random_number<int>() << '\n';
std::cout << ru::random_number(10) << '\n';
std::cout << ru::random_number(10, 20) << '\n';

```
/*
 * test.cpp
 *
 *  Created on: 1 Jul 2018
 *      Author: galik
 */

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <mutex>
#include <thread>
#include <vector>
#include <iostream>

#include <randomly_useful.hpp>

namespace ru = randomly_useful;

TEST_CASE("Randomly Useful Seeding", "[seeding]")
{
	SECTION("auto_minstd_prng")
	{
		ru::auto_minstd_prng rng;
		rng.seed(1);
		REQUIRE(rng.random_number<std::int32_t>() == 182605793);
	}

	SECTION("auto_mt19937_prng")
	{
		ru::auto_mt19937_prng rng;
		rng.seed(1);
		REQUIRE(rng.random_number<std::int32_t>() == 1791095845);
	}

	SECTION("auto_mt19937_64_prng")
	{
		ru::auto_mt19937_64_prng rng;
		rng.seed(1);
		REQUIRE(rng.random_number<std::int32_t>() == 287497903);
	}

	SECTION("auto_ranlux24_prng")
	{
		ru::auto_ranlux24_prng rng;
		rng.seed(1);
		REQUIRE(rng.random_number<std::int32_t>() == 1127814431);
	}

	SECTION("auto_ranlux48_prng")
	{
		ru::auto_ranlux48_prng rng;
		rng.seed(1);
		REQUIRE(rng.random_number<std::int32_t>() == 177182603);
	}

	// static

	SECTION("static_minstd_prng")
	{
		ru::static_minstd_prng rng;
		rng.seed(1);
		REQUIRE(rng.random_number<std::int32_t>() == 182605793);
	}

	SECTION("static_mt19937_prng")
	{
		ru::static_mt19937_prng rng;
		rng.seed(1);
		REQUIRE(rng.random_number<std::int32_t>() == 1791095845);
	}

	SECTION("static_mt19937_64_prng")
	{
		ru::static_mt19937_64_prng rng;
		rng.seed(1);
		REQUIRE(rng.random_number<std::int32_t>() == 287497903);
	}

	SECTION("static_ranlux24_prng")
	{
		ru::static_ranlux24_prng rng;
		rng.seed(1);
		REQUIRE(rng.random_number<std::int32_t>() == 1127814431);
	}

	SECTION("static_ranlux48_prng")
	{
		ru::auto_ranlux48_prng rng;
		rng.seed(1);
		REQUIRE(rng.random_number<std::int32_t>() == 177182603);
	}

	// thread_local

	SECTION("thread_local_minstd_prng")
	{
		ru::thread_local_minstd_prng rng;
		rng.seed(1);
		REQUIRE(rng.random_number<std::int32_t>() == 182605793);
	}

	SECTION("thread_local_mt19937_prng")
	{
		ru::thread_local_mt19937_prng rng;
		rng.seed(1);
		REQUIRE(rng.random_number<std::int32_t>() == 1791095845);
	}

	SECTION("thread_local_mt19937_64_prng")
	{
		ru::thread_local_mt19937_64_prng rng;
		rng.seed(1);
		REQUIRE(rng.random_number<std::int32_t>() == 287497903);
	}

	SECTION("thread_local_ranlux24_prng")
	{
		ru::thread_local_ranlux24_prng rng;
		rng.seed(1);
		REQUIRE(rng.random_number<std::int32_t>() == 1127814431);
	}

	SECTION("thread_local_ranlux48_prng")
	{
		ru::auto_ranlux48_prng rng;
		rng.seed(1);
		REQUIRE(rng.random_number<std::int32_t>() == 177182603);
	}
}

TEST_CASE("Copying", "[copy]")
{
	SECTION("auto_mt19937_prng")
	{
		ru::auto_mt19937_prng rng1;
		auto rng2 = rng1;

		std::vector<int> v1;
		std::vector<int> v2;

		std::generate_n(std::back_inserter(v1), 100, [&]{ return rng1.random_number(0, 99); });
		std::generate_n(std::back_inserter(v2), 100, [&]{ return rng2.random_number(0, 99); });

		REQUIRE(v1 == v2);
	}

	SECTION("static_mt19937_prng")
	{
		ru::static_mt19937_prng rng1;
		auto rng2 = rng1;

		std::vector<int> v1;
		std::vector<int> v2;

		std::generate_n(std::back_inserter(v1), 100, [&]{ return rng1.random_number(0, 99); });
		std::generate_n(std::back_inserter(v2), 100, [&]{ return rng2.random_number(0, 99); });

		REQUIRE(v1 != v2);
	}

	SECTION("thread_local_mt19937_prng")
	{
		{
			ru::thread_local_mt19937_prng rng1;
			auto rng2 = rng1;

			std::vector<int> v1;
			std::vector<int> v2;

			std::generate_n(std::back_inserter(v1), 100, [&]{ return rng1.random_number(0, 99); });
			std::generate_n(std::back_inserter(v2), 100, [&]{ return rng2.random_number(0, 99); });

			REQUIRE(v1 != v2);
		}
//		{
//			std::vector<int> v1;
//			std::vector<int> v2;
//
//			auto t1 = std::thread([&]{
//				ru::thread_local_mt19937_prng rng;
//				rng.seed(1);
//				std::generate_n(std::back_inserter(v1), 100, [&]{ return rng.random_number(0, 99); });
//			});
//
//			auto t2 = std::thread([&]{
//				ru::thread_local_mt19937_prng rng;
//				rng.seed(1);
//				std::generate_n(std::back_inserter(v2), 100, [&]{ return rng.random_number(0, 99); });
//			});
//
//			t1.join();
//			t2.join();
//
//			REQUIRE(v1 == v2);
//		}
	}
}

TEST_CASE("Randomly Useful uniform numbers", "[uniform]")
{
	SECTION("auto_mt19937_prng")
	{
		ru::auto_mt19937_prng rng;

		int min = std::numeric_limits<int>::max();
		int max = std::numeric_limits<int>::lowest();
		int total = 0;
		int n = 0;

		for(; n < 1'000'000; ++n)
		{
			auto rn = rng.random_number(-100, 100);

			if(rn < min)
				min = rn;

			if(rn > max)
				max = rn;

			total += rn;
		}

		double average = double(total) / double(n);

		REQUIRE(min >= -100);
		REQUIRE(max <=  100);
		REQUIRE(average > -0.2);
		REQUIRE(average <  0.2);
	}
}
